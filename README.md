# imageTp0

## Authors and acknowledgment
Oussama Benaziz p2007990, Jules Bonhotal - p2003042

## Links
Le TP0 peut être téléchargé via la commande "git clone https://forge.univ-lyon1.fr/p2003042/imagetp0rendu.git".
Il se trouve sur la forge à l'adresse : "https://forge.univ-lyon1.fr/p2003042/imagetp0rendu".

## Compilation
Le TP peut être compilé via la commande "make", qui génère un exécutable "tp0". Cet exécutable prend en argument le chemin vers la photo sur laquelle les traitements seront effectués. Par exemple, "./tp0 imageSujet.png".

## Fichiers

- **imageSujet.png** : une image servant d'exemple à traiter.
- **Makefile** : le makefile permettant de compiler le TP.
- **rapportImageAnalyse.pdf** : le rapport avec la description du travail effectué et des images en exemples.
- **README.md** : ce fichier.
- **tp0.cpp** : le code du TP.

