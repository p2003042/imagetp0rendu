CXX = g++
CXXFLAGS = -Wall -Wextra -std=c++11

# Adjust the path if necessary
OPENCV_LIBS = `pkg-config --cflags --libs opencv4`

tp0: tp0.cpp
	$(CXX) $(CXXFLAGS) -o tp0 tp0.cpp $(OPENCV_LIBS)

.PHONY: clean

clean:
	rm -f tp0
