#include <opencv2/opencv.hpp>
#include <vector>

std::vector<int> calculateHistogram(const cv::Mat& image) {
    cv::Mat grayImage;
    if (image.channels() > 1) {
        cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = image.clone();
    }

    int histSize = 256;
    float range[] = {0, 256};
    const float* histRange = {range};

    cv::Mat hist;
    cv::calcHist(&grayImage, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange);

    std::vector<int> histogram;
    for (int i = 0; i < histSize; ++i) {
        histogram.push_back(static_cast<int>(hist.at<float>(i)));
    }

    return histogram;
}

std::vector<int> calculateHistogramManual(const cv::Mat& image) {
    cv::Mat grayImage;
    if (image.channels() > 1) {
        cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = image.clone();
    }

    const int histSize = 256;

    std::vector<int> histogram(histSize, 0);

    for (int y = 0; y < grayImage.rows; ++y) {
        for (int x = 0; x < grayImage.cols; ++x) {
            int pixelValue = static_cast<int>(grayImage.at<uchar>(y, x));
            histogram[pixelValue]++;
        }
    }

    return histogram;
}

cv::Mat drawHistogram(const std::vector<int>& histogram) {
    int maxCount = *std::max_element(histogram.begin(), histogram.end());

    int histWidth = 512;
    int histHeight = 400;
    cv::Mat histImage(histHeight, histWidth, CV_8UC3, cv::Scalar(0, 0, 0));

    int binWidth = histWidth / histogram.size();

    for (int i = 0; i < histogram.size(); ++i) {
        int barHeight = static_cast<int>(histHeight * static_cast<float>(histogram[i]) / maxCount);
        cv::rectangle(histImage, cv::Point(i * binWidth, histHeight - 1),
                      cv::Point((i + 1) * binWidth - 1, histHeight - barHeight),
                      cv::Scalar(255, 255, 255), cv::FILLED);
    }

    // Afficher l'image de l'histogramme
    return histImage;
}

void findMinMax(const cv::Mat& image, double& minValue, double& maxValue) {
    minValue = DBL_MAX;
    maxValue = -DBL_MAX;

    for (int y = 0; y < image.rows; ++y) {
        for (int x = 0; x < image.cols; ++x) {
            double pixelValue = static_cast<double>(image.at<uchar>(y, x));
            minValue = std::min(minValue, pixelValue);
            maxValue = std::max(maxValue, pixelValue);
        }
    }
}

void stretchImageIntensity(const cv::Mat& inputImage, cv::Mat& outputImage, double a, double b, double minVal, double maxVal) {
    outputImage.create(inputImage.size(), inputImage.type());

    for (int y = 0; y < inputImage.rows; ++y) {
        for (int x = 0; x < inputImage.cols; ++x) {
            double inputValue = static_cast<double>(inputImage.at<uchar>(y, x));
            double outputValue = (b - a) * ((inputValue - minVal) / (maxVal - minVal)) + a;
            outputImage.at<uchar>(y, x) = static_cast<uchar>(cv::saturate_cast<uchar>(outputValue));
        }
    }
}

cv::Mat stretchHistogram(const cv::Mat& inputImage, int a, int b) {
    cv::Mat grayImage;
    if (inputImage.channels() > 1) {
        cv::cvtColor(inputImage, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = inputImage.clone();
    }

    double minVal, maxVal;
    findMinMax(grayImage, minVal, maxVal);

    cv::Mat stretchedImage;
    stretchImageIntensity(grayImage, stretchedImage, a, b, minVal, maxVal);

    return stretchedImage;
}

cv::Mat normalizeHistogram(const cv::Mat& inputImage, double D) {
    cv::Mat grayImage;
    if (inputImage.channels() > 1) {
        cv::cvtColor(inputImage, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = inputImage.clone();
    }

    std::vector<int> histogram = calculateHistogramManual(grayImage);

    double minVal, maxVal;
    findMinMax(grayImage, minVal, maxVal);

    double scalingFactor = D / histogram.size();

    cv::Mat normalizedImage = grayImage.clone();

    for (int y = 0; y < grayImage.rows; ++y) {
        for (int x = 0; x < grayImage.cols; ++x) {
            int pixelValue = static_cast<int>(grayImage.at<uchar>(y, x));
            double normalizedValue = scalingFactor * pixelValue;
            normalizedImage.at<uchar>(y, x) = static_cast<uchar>(cv::saturate_cast<uchar>(normalizedValue));
        }
    }

    return normalizedImage;
}



cv::Mat equalizeHistogram(const cv::Mat& inputImage, double D) {
    cv::Mat grayImage;
    if (inputImage.channels() > 1) {
        cv::cvtColor(inputImage, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = inputImage.clone();
    }

    std::vector<int> histogram = calculateHistogramManual(grayImage);

    std::vector<int> cumulativeHistogram(histogram.size());
    cumulativeHistogram[0] = histogram[0];
    for (int i = 1; i < histogram.size(); ++i) {
        cumulativeHistogram[i] = cumulativeHistogram[i - 1] + histogram[i];
    }

    double scalingFactor = D / (grayImage.rows * grayImage.cols);

    cv::Mat equalizedImage = grayImage.clone();

    for (int y = 0; y < grayImage.rows; ++y) {
        for (int x = 0; x < grayImage.cols; ++x) {
            int pixelValue = static_cast<int>(grayImage.at<uchar>(y, x));
            double equalizedValue = scalingFactor * cumulativeHistogram[pixelValue];
            equalizedImage.at<uchar>(y, x) = static_cast<uchar>(cv::saturate_cast<uchar>(equalizedValue));
        }
    }


    return equalizedImage;
}



cv::Mat applyFilter(const cv::Mat& inputImage, const std::vector<std::vector<int>>& filter) {
    cv::Mat grayImage;
    if (inputImage.channels() > 1) {
        cv::cvtColor(inputImage, grayImage, cv::COLOR_BGR2GRAY);
    } else {
        grayImage = inputImage.clone();
    }

    cv::Mat outputImage = grayImage.clone();

    for (int y = 1; y < grayImage.rows - 1; ++y) {
        for (int x = 1; x < grayImage.cols - 1; ++x) {
            int sum = 0;
            for (int i = -1; i <= 1; ++i) {
                for (int j = -1; j <= 1; ++j) {
                    int pixelValue = static_cast<int>(grayImage.at<uchar>(y + i, x + j));
                    sum += pixelValue * filter[i + 1][j + 1];
                }
            }
            outputImage.at<uchar>(y, x) = static_cast<uchar>(cv::saturate_cast<uchar>(sum));
        }
    }

    return outputImage;
}


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <image_path>" << std::endl;
        return -1;
    }

    const cv::Mat originalImage = cv::imread(argv[1]);

    if (originalImage.empty()) {
        std::cerr << "Could not open or find the image." << std::endl;
        return -1;
    }

    int Nmin = 0;
    int Nmax = 255;
    int a = 100;
    int b = 150;
    
    double D = 100.0;


    double D2 = 200.0;
    
    std::vector<std::vector<int>> filter = {
        {0, -1, 0},
        {-1, 4, -1},
        {0, -1, 0}
    };

    cv::Mat stretchedImage = stretchHistogram(originalImage, a, b);
    
    cv::Mat normalizedImage = normalizeHistogram(originalImage, D);
    
    cv::Mat equalizedImage = equalizeHistogram(originalImage, D2);
    
    cv::Mat filteredImage = applyFilter(originalImage, filter);

    std::vector<int> histogramOriginal = calculateHistogramManual(originalImage);
    std::vector<int> histogramStretched = calculateHistogramManual(stretchedImage);
    std::vector<int> histogramNormalized = calculateHistogramManual(normalizedImage);
    std::vector<int> histogramEqualized = calculateHistogramManual(equalizedImage);
    std::vector<int> histogramFiltered = calculateHistogramManual(filteredImage);

    cv::Mat histImageOriginal = drawHistogram(histogramOriginal);
    cv::Mat histImageStretched = drawHistogram(histogramStretched);
    cv::Mat histImageNormalized = drawHistogram(histogramNormalized);
    cv::Mat histImageEqualized = drawHistogram(histogramEqualized);
    cv::Mat histImageFiltered = drawHistogram(histogramFiltered);

    cv::imshow("Original Image", originalImage);
    cv::imshow("Stretched Image", stretchedImage);
    cv::imshow("Normalized Image", normalizedImage);
    cv::imshow("Equalized Image", equalizedImage);
    cv::imshow("Filtered Image", filteredImage);

    cv::imshow("Histogram Original", histImageOriginal);
    cv::imshow("Histogram Stretched", histImageStretched);
    cv::imshow("Histogram Normalized", histImageNormalized);
    cv::imshow("Histogram Equalized", histImageEqualized);
    cv::imshow("Histogram Filtered", histImageFiltered);

    cv::waitKey(0);

    return 0;
}